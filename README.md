# Telegram bot with mysql implementation proof of concept

## Installation

1. git clone blablabla  
2. cp .env.example .env
3. // edit bot token inside file .env
4. npm install
5. npm start

## Database example schema

| id | name   | color |
| -- | ------ | ----- |
| A1 | Nissan | Red   |
| C3 | Honda  | Blue  |
| E4 | Suzuki | Black |
