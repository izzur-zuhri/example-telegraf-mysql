require('dotenv').config()

const express = require('express')
const path = require('path')
const PORT = 5000
const Telegraf = require('telegraf')
const bot = new Telegraf(process.env.BOT_TOKEN)
const mysql = require('mysql')

express().listen(PORT, () => console.log(`Listening on ${PORT}`))

const connection = mysql.createConnection({
  host      : 'localhost',
  user      : 'test',
  password  : 'secret',
  database  : 'my_test_db'
})

function selectCar(ctx) {
  connection.connect()
  connection.query('SELECT * FROM car WHERE id=1', (error, result, fields) => {
    if (error) throw error
    console.log(result)
    ctx.reply(result[0])
    connection.end()
  })
} 

bot.start((ctx) => ctx.reply('Welcome!'))
bot.help((ctx) => ctx.reply('Send me a sticker'))
bot.on('sticker', (ctx) => ctx.reply('👍'))
bot.hears('hi', (ctx) => ctx.reply('Hey there'))
bot.hears(/buy/i, (ctx) => ctx.reply('Buy-buy'))
bot.command(['/car', 'car'], ctx => selectCar(ctx))

bot.startPolling()

